sender.py
--------------

For using the sender you just need to change the source address and the destination address. The source address is the address of the 
machine that will be run and the destination is the address where the client runs. In addition you can just change the offset
to different values depending on how much small/large you want your chunks to be. Finally you will have to edit the line that holds
the file that you want to send with the file in the local directory of the sender.

sniffer.py
--------------

You will have to change the IP address of the remote server (that runs the sender.py) so that it will filter the packets and will grab
only those who are coming from the specific host. Nothing else is really needed. 

Planned Features
--------------
- Encryption

- Progress Bar


License
--------------
Runs under GNU GPL v3
http://www.gnu.org/licenses/gpl-3.0.txt


Notice
--------------
Do not use it for any malicious or illegal activity. 